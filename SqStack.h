#pragma once
#include<stdio.h>
#include<stdlib.h>


#define MaxSize 10
#define FALSE 0
#define TRUE 1

typedef int Elem;
typedef struct
{
	Elem data[MaxSize];
	int top;
}SqStack;
//��ʼ��
void InitStack(SqStack* S);
//�ж��Ƿ�Ϊ��ջ
int Empty(SqStack S);
//��ջ
int Push(SqStack* S, Elem x);
//��ջ
int Pop(SqStack* S, Elem* x);
//��ȡջ��Ԫ��
int GetTop(SqStack* S, Elem* x);