#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//单词倒排
int main()
{
	char arr[100][22] = { 0 };
	int x = 0, i = 0, j = 0;
	while (1)
	{
		//scanf的返回值有三种，输入格式正确返回1，格式不正确返回0，输入结束返回EOF
		x = scanf("%[a-z|A-Z]", arr[i]);
		//如果接收到\n则表示字符串输入结束
		if (getchar() == '\n')
			break;
		//如果接受到非字母的字符之后，保存到二维数组的下一行
		if (x)
		{
			i++;
		}
	}
	for (j = i; j >= 0;j--)
	{
		printf("%s ", arr[j]);
	}
	return 0;
}

//数字颠倒
//void ReversalNumber(char arr[11], int n)
//{
//	if (n == 0)
//	{
//		arr[0] = '0';
//	}
//	int count = 0;
//	int tmp = n;
//	while (tmp)
//	{
//		tmp = tmp / 10;
//		count++;
//	}
//	int i = 0;
//	for (i = 0 ;i < count ;i++)
//	{
//		arr[i] = (n % 10) + '0';
//		n = n / 10;
//	}
//}
//int main()
//{
//	int n = 0;
//	while (~scanf("%d", &n))
//	{
//		char number[11] = { 0 };
//		ReversalNumber(number, n);
//		printf("%s\n", number);
//	}
//	return 0;
//}

//完全数计算
//完全数（Perfect number），又称完美数或完备数，是一些特殊的自然数。
//它所有的真因子（即除了自身以外的约数）的和（即因子函数），恰好等于它本身。
//int PerfectNumber(int n)
//{
//    int count = 0;
//    for (int i = 1;i <= n;i++)
//    {
//        int sum = 0;
//        int j = 0;
//        for (j = 1;j < i;j++)
//        {
//            if (i % j == 0)
//            {
//                sum += j;
//            }
//        }
//        if (sum == i)
//        {
//            count++;
//        }
//    }
//    return count;
//}
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        printf("%d\n", PerfectNumber(n));
//    }
//    return 0;
//}

//最大连续 1 的个数
//int findMaxConsecutiveOnes(int* nums, int numsSize)
//{
//    int i = 0, j = 0;
//    int num[100000] = { 0 };
//    for (i = 0; i < numsSize;i++)
//    {
//        if (nums[i] == 1)
//        {
//            num[j]++;
//        }
//        else
//        {
//            j++;
//        }
//    }
//    int max = 0;
//    for (i = 0;i <= j;i++)
//    {
//        if (num[i] > max)
//            max = num[i];
//    }
//    return max;
//}
//int main()
//{
//    int nums[100000] = { 0 };
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        int i = 0;
//        for (i = 0;i < n;i++)
//        {
//            scanf("%d", &nums[i]);
//        }
//        printf("%d", findMaxConsecutiveOnes(nums, n));
//    }
//	return 0;
//}