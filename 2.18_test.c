#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//调整数组使奇数全部都位于偶数前面
void Exchange(int arr[100], int n)
{
	int temp = 0;
	int i = 0, j = 0;
	for (i = 0; i < n;i++)
	{
		if (arr[i] % 2 == 1)
		{
			temp = arr[j];
			arr[j] = arr[i];
			arr[i] = temp;
			j++;
		}
	}
}
int main()
{
	int arr[100] = { 0 };
	int n = 0;//数组元素的个数
	while (~scanf("%d", &n))
	{
		int i = 0;
		for (i = 0; i < n;i++)
		{
			scanf("%d", &arr[i]);
		}
		Exchange(arr, n);
		for (i = 0; i < n;i++)
		{
			printf("%d ", arr[i]);
		}
	}
	return 0;
}


//喝汽水问题
//int main()
//{
//	int n = 0;
//	while (~scanf("%d", &n))
//	{
//		int count = 0;
//		count = n;
//		while (n > 1)
//		{
//			count += (n / 2);
//			n = n / 2 + n % 2;
//		}
//		printf("%d\n", count);
//	}
//	return 0;
//}