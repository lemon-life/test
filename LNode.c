#define _CRT_SECURE_NO_WARNINGS 1
#include"LNode.h"

//初始化单链表
int InitList(LinkList L)
{
	L = (LNode*)malloc(sizeof(LNode));
	if (L == NULL)
		return FALSE;
	L->next = NULL;
	return TRUE;
}
//判断单链表是否为空
int Empty(LinkList L)
{
	if (L->next == NULL)
	{
		return TRUE;
	}
	return FALSE;
}
//按位查找
LNode* GetElem(LinkList L, int i)
{
	if (i < 0)
		return NULL;
	int j = 0;
	LNode* p = L;
	while (p != NULL && j < i)
	{
		p = p->next;
		j++;
	}
	return p;
}
//按值查找
LNode* LocateElem(LinkList L, ElemType e)
{
	LNode* p = L;
	while (p != NULL && p->data != e)
	{
		p = p->next;
	}
	return p;
}
//插入元素
int ListInsert(LinkList L, int i, ElemType e)
{
	if (i < 1)
		return FALSE;
	LNode* p = (LNode*)malloc(sizeof(LNode));
	p = GetElem(L, i);
	if (InsertNextNode(p, e))
		return TRUE;
	return FALSE;
}
//后插操作
int InsertNextNode(LNode* p, ElemType e)
{
	if (p == NULL)
		return FALSE;
	LNode* s = (LNode*)malloc(sizeof(LNode));
	if (s == NULL)
		return FALSE;
	s->data = e;
	s->next = p->next;
	p->next = s;
	return TRUE;
}
//前插操作
int InsertPriorNode(LNode* p, ElemType e)
{
	if (p == NULL)
		return FALSE;
	LNode* s = (LNode*)malloc(sizeof(LNode));
	if (s == NULL)
		return FALSE;
	s->data = p->data;
	p->data = e;
	s->next = p->next;
	p->next = s;
	return TRUE;
}
//按位删除元素
int ListDelete(LinkList L, int i, ElemType* e)
{
	LNode* p = L;
	int j = 0;
	while (p != NULL && j < i - 1)
	{
		p = p->next;
		j++;
	}
	if (p == NULL)
		return FALSE;
	if (p->next == NULL)
		return FALSE;
	LNode* q = p->next;
	*e = q->data;
	p->next = q->next;
	free(q);
	return TRUE;
}
//删除指定节点
int DeleteNode(LNode* p)
{
	if (p == NULL)
		return FALSE;
	if (p->next != NULL)
	{
		LNode* q = p->next;
		p->data = q->data;
		p->next = q->next;
		free(q);
		return TRUE;
	}
	//若p为最后一个节点则产生bug，无法删除
}
//求表长
int Length(LinkList L)
{
	int len = 0;
	LNode* p = L;
	while (p->next != NULL)
	{
		p = p->next;
		len++;
	}
	return len;
}