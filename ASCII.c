#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//字符串逆序（递归实现）
//int my_strlen1(char* str)
//{
//	if(*str != '\0')
//	{
//		str++;
//		return (1 + my_strlen1(str));
//	}
//	return 0;
//}


//递归和非递归分别实现strlen
//递归
//int my_strlen1(char* str)
//{
//	if(*str != '\0')
//	{
//		str++;
//		return (1 + my_strlen1(str));
//	}
//	return 0;
//}
////非递归
//int my_strlen2(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	printf("%d\n", my_strlen1(arr));
//	printf("%d\n", my_strlen2(arr));
//	return 0;
//}

//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//int factorial(int n)
//{
//	if (n == 1)
//		return 1;
//	else
//	    return n * factorial(n - 1);
//} 
//int main()
//{
//	int n = 0;
//	printf("请输入要求的阶乘:>\n");
//	scanf("%d", &n);
//	printf("%d的阶乘 = %d\n", n, factorial(n));
//	return 0;
//}

//递归方式实现打印一个整数的每一位
//void print(int n)
//{
//	if (n > 9)
//	{
//		print(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//int main()
//{
//	int num = 0;
//	printf("输入一个整数:>\n");
//	scanf("%d", &num);
//	print(num);
//	return 0;
//}

//实现字母的大小写转换。多组输入输出。
//int main()
//{
//    char x = '0';
//    while ((x = getchar()) != EOF)
//    {
//        getchar();
//        if (x >= 'A' && x <= 'Z')
//        {
//            printf("%c\n", x + 32);
//        }
//        else if (x >= 'a' && x <= 'z')
//        {
//            printf("%c\n", x - 32);
//        }
//    }
//    return 0;
//}
//小乐乐排电梯
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int min = 0;
//    min = (n / 12) * 4 + 2;
//    printf("%d\n", min);
//    return 0;
//}

//转换ASCII码
//int main()
//{
//    char arr[] = { 73, 32, 99, 97, 110, 32, 100, 111, 32, 105, 116 , 33 };
//    int sz = sizeof(arr) / sizeof(arr[0]);
//    for (int i = 0; i < sz;i++)
//    {
//        printf("%c", arr[i]);
//    }
//    return 0;
//}