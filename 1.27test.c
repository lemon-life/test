#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//数字在升序数组中出现的次数
//解法一：二分查找
int GetNumberOfK(int* data, int dataLen, int k)
{
	// write code here
	int left = 0;
	int i = 1, j = 1;
	int right = dataLen - 1;
	while (left <= right)
	{
		int mid = left + (right - left) / 2;
		if (data[mid] > k)
		{
			right = mid - 1;
		}
		else if (data[mid] < k)
		{
			left = mid + 1;
		}
		else
		{
			while (data[mid - i] == k)
			{
				i++;
			}
			while (data[mid + j] == k)
			{
				j++;
			}
			break;
		}
	}
	if (left > right)
		return 0;
	else
		return (j + i - 1);
}
//解法二，遍历数组
int GetNumberOfK2(int* data, int dataLen, int k)
{
	int i = 0;
	int count = 0;
	for (i = 0;i < dataLen;i++)
	{
		if (data[i] > k)
			break;
		if (data[i] == k)
			count++;
	}
	return count;
}
int main()
{
	int arr[8] = { 1,2,3,3,3,3,4,5 };
	printf("%d", GetNumberOfK2(arr, 8, 3));
	return 0;
}


//密码检查
//1. 密码只能由大写字母，小写字母，数字构成；
//2. 密码不能以数字开头；
//3. 密码中至少出现大写字母，小写字母和数字这三种字符类型中的两种；
//4. 密码长度至少为8
//int main()
//{
//	char password[100] = { 0 };
//	int n = 0;
//	scanf("%d", &n);
//	for (int j = 0;j < n;j++)
//	{
//		scanf("%s", password);
//		int flag1 = 0;
//		int flag2 = 0;
//		int flag3 = 0;
//		int sz = strlen(password);
//		if (sz < 8 || (password[0] >= '0' && password[0] <= '9'))
//		{
//			printf("NO\n");
//		}
//		else
//		{
//			for (int i = 0;i < sz;i++)
//			{
//				if (password[i] >= '0' && password[i] <= '9')
//				{
//					flag1 = 1;
//				}
//				if (password[i] >= 'a' && password[i] <= 'z')
//				{
//					flag2 = 1;
//				}
//				if (password[i] >= 'A' && password[i] <= 'Z')
//				{
//					flag3 = 1;
//				}
//			}
//			if (flag1 + flag2 + flag3 > 1)
//			{
//				printf("YES\n");
//			}
//			else
//				printf("NO\n");
//		}
//	}
//	return 0;
//}