#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define ROW 3
#define COL 3

//初始化数组
void InitBoard(char board[ROW][COL], int row, int col);

//打印棋盘
void DisplayBoard(char board[ROW][COL], int row, int col);

//玩家走
void PlayerMove(char board[ROW][COL], int row, int col);

//电脑走（随机）
void ComputerMove(char board[ROW][COL], int row, int col);

//判断棋盘是否已满
int IsFull(char board[ROW][COL], int row, int col);

//判断游戏是否结束
char GameOver(char board[ROW][COL], int row, int col);