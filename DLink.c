#define _CRT_SECURE_NO_WARNINGS 1
#include"DLink.h"

//初始化
int InitLink(DLinkList* L)
{
	*L = (DLinkList)malloc(sizeof(DNode));
	if (!(*L))
		return FALSE;
	(*L)->next = NULL;
	(*L)->prior = NULL;
	return TRUE;
}
//判断是否为空
int Empty(DLinkList L)
{
	if (L->next == NULL)
		return 1;
	return 0;
}
//按位查找
DNode* GetElem(DLinkList L, int i)
{
	if (i < 1)
		return NULL;
	int j = 0;
	DNode* p = L;
	while (p != NULL && j < i)
	{
		p = p->next;
		j++;
	}
	return p;
}
//按值查找
DNode* LocateElem(DLinkList L, ElemType e)
{
	DNode* p = L;
	while (p != NULL && p->data != e)
	{
		p = p->next;
	}
	return p;
}
//后插法
int InsertNextList(DNode* p, DNode* s)
{
	if (p == NULL || s == NULL)
		return FALSE;
	s->next = p->next;
	if (p->next != NULL)
	{
		p->next->prior = s;
	}
	p->next = s;
	s->prior = p;
	return TRUE;
}
//前插法
int InsertPriorList(DNode* p, DNode* s)
{
	if (p == NULL || s == NULL)
		return FALSE;
	s->next = p;
	if (p->prior != NULL)
		p->prior->next = s;
	s->prior = p->prior;
	p->prior = s;
	return TRUE;
}
//删除第i个节点
int Delete(DLinkList* L, int i, ElemType* e)
{
	DNode* p = L;
	int j = 0;
	while (p != NULL && j < i - 1)
	{
		p = p->next;
		j++;
	}
	if (p == NULL)
		return FALSE;
	DNode* q = p->next;
	if (q == NULL)
		return FALSE;
	p->next = q->next;
	if (q->next != NULL)
	{
		q->next->prior = p;
	}
	*e = q->data;
	free(q);
	return TRUE;
}
