#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

//两个数组的交集
//比较函数
int cmp(const void* a, const void* b)
{
    return (*(int*)a - *(int*)b);
}
//两个数组的交集
int* intersection(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize)
{
    //快速将两个数组按升序排序
    qsort(nums1, nums1Size, sizeof(int), cmp);
    qsort(nums2, nums2Size, sizeof(int), cmp);
    *returnSize = 0;
    int index1 = 0, index2 = 0;
    //交集的元素个数一定不会超过两个一维数组的最多元素个数
    int* p = (int*)malloc((nums1Size > nums2Size ? nums1Size : nums2Size) * sizeof(int));
    //从大到小比较
    while ((index1 < nums1Size) && (index2 < nums2Size))
    {
        if (nums1[index1] == nums2[index2])
        {
            //避免出现重复的元素，或运算前方未真则不继续执行后方运算
            if (!(*returnSize) || nums1[index1] != p[(*returnSize) - 1])
            {
                p[(*returnSize)++] = nums1[index1];
            }
            index1++;
            index2++;
        }
        else if (nums1[index1] > nums2[index2])
            index2++;
        else
            index1++;
    }
    return p;
}
int main()
{
    int num1[100] = { 0 };
    int num2[100] = { 0 };
    int n1 = 0, n2 = 0;
    int arr[100] = { 0 };
    int* pa = arr;
    while (~scanf("%d %d", &n1, &n2))
    {
        int i = 0, j = 0;
        for (i = 0;i < n1;i++)
        {
            scanf("%d", &num1[i]);
        }
        for (j = 0; j < n2;j++)
        {
            scanf("%d", &num2[j]);
        }
        printf("[");
        pa = intersection(num1, n1, num2, n2, pa);
        while (*pa > 0 && *pa < 1001)
        {
            printf("%d", *pa);
            pa++;
            if (*pa > 0 && *pa < 1001)
                printf(",");
        }
        printf("]\n");
    }
	return 0;
}

//至少是其他数字两倍的最大数
//int dominantIndex(int* nums, int numsSize)
//{
//	int max = nums[0];
//	int max2 = 0;
//	int flag = 0;
//	if (numsSize == 1)
//		return 0;
//	else if (numsSize == 2)
//	{
//		if (nums[1] >= max)
//		{
//			max = nums[1];
//			max2 = nums[0];
//			flag = 1;
//		}
//		else
//		{
//			max2 = nums[1];
//		}
//		if (max >= (2 * max2))
//			return flag;
//		else
//			return -1;
//	}
//	else
//	{
//		if (nums[1] >= max)
//		{
//			max = nums[1];
//			max2 = nums[0];
//			flag = 1;
//		}
//		else
//		{
//			max2 = nums[1];
//		}
//		for (int i = 2;i < numsSize;i++)
//		{
//			if (nums[i] >= max)
//			{
//				max2 = max;
//				max = nums[i];
//				flag = i;
//			}
//			else
//			{
//				if (nums[i] >= max2)
//				{
//					max2 = nums[i];
//				}
//			}
//		}
//	}
//	if (max >= (2 * max2))
//		return flag;
//	else
//		return -1;
//}
//int main()
//{
//	int nums[100] = { 0 };
//	int n = 0;
//	while (~scanf("%d", &n))
//	{
//		int i = 0;
//		for (i = 0; i < n; i++)
//		{
//			scanf("%d", &nums[i]);
//		}
//		printf("%d", dominantIndex(nums, n));
//	}
//	return 0;
//}

//计算两个整数二进制不同的位数
//int convertInteger(int A, int B) 
//{
//    int count = 0;
//    for (int i = 0;i < 32;i++)
//    {
//        if (((A ^ B) >> i) & 1)
//            count++;
//    }
//    return count;
//}
//int main()
//{
//    int a = 29;
//    int b = 15;
//    printf("%d", convertInteger(a, b));
//    return 0;
//}


//错误的集合
//int* findErrorNums(int* nums, int numsSize, int* returnSize) {
//    int i = 0;
//    *returnSize = 2;
//    int* p = (int*)malloc(2 * sizeof(int));
//    //创建一个指针并分配numsSize个空间(int)给他
//    int* arr = (int*)malloc((numsSize) * sizeof(int));
//    //将指针arr的数据域初始化为0
//    memset(arr, 0, (numsSize) * sizeof(int));
//    //循环，若nums数组中的元素未错误，则arr[i] = 1,若有位置错误， 则错误位置的arr[i] = 0
//    for (int i = 0;i < numsSize;i++)
//        arr[nums[i] - 1]++;
//    for (int i = 0;i < numsSize;i++)
//    {
//        //arr[i] = 2的话，则可以找到错误的元素
//        if (arr[i] == 2) 
//            p[0] = i + 1;
//        //arr[i] = 0的话，可以找到错误的位置
//        if (arr[i] == 0)
//            p[1] = i + 1;
//    }
//    return p;
//}
//int main()
//{
//    int num[100] = { 0 };
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        int str[2] = { 0 };
//        int i = 0;
//        int* p = str;
//        for ( i = 0; i < n; i++)
//        {
//            scanf("%d", &num[i]);
//        }
//        p = findErrorNums(num, n, str);
//        printf("[%d,%d]\n", *p, *(p + 1));
//    }
//    return 0;
//}
