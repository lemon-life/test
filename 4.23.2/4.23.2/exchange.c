#define _CRT_SECURE_NO_WARNINGS 1
//��ת����
void reserve(int* nums, int left, int right)
{
    assert(nums);
    while (left < right) {
        int tmp = nums[left];
        nums[left] = nums[right];
        nums[right] = tmp;
        left++;
        right--;
    }
}
void rotate(int* nums, int numsSize, int k) {
    if (k > numsSize) {
        k %= numsSize;
    }
    reserve(nums, 0, numsSize - k - 1);
    reserve(nums, numsSize - k, numsSize - 1);
    reserve(nums, 0, numsSize - 1);
}