#pragma once
#include<stdio.h>
#include<stdlib.h>

#define FALSE 0
#define TRUE 1
typedef int Elem;
typedef struct LinkNode
{
	Elem data;
	struct LinkStack* next;
}LiStack;

//��ʼ����ջ
int InitLiStack(LiStack* S);
//��ջ
int Push(LiStack* S, Elem e);
//��ջ
int Pop(LiStack* S, Elem* e);
//ȡջ��Ԫ��
int GetTop(LiStack* S, Elem* e);
//����
void PrintStack(LiStack* S);