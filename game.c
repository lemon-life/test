#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

//初始化数组
void InitBoard(char board[ROW][COL], int row, int col)
{
	//初始化将数组元素置为空格
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			board[i][j] = ' ';
		}
	}
}

//打印棋盘
void DisplayBoard(char board[ROW][COL], int row, int col)
{
	int i = 0, j = 0;
	//将棋盘拆分为九块来打印
	for (i = 0; i < row;i++)
	{
		for (j = 0;j < col;j++)
		{
			printf(" %c ", board[i][j]);
			//每行最后一次不打印 |
			if (j < col - 1)
				printf("|");
		}
		printf("\n");
		//棋盘分隔处只打印两次
		if (i < row - 1)
		{
			for (j = 0;j < col;j++)
			{
				printf("---");
				//每行最后一次不打印 |
				if (j < col - 1)
					printf("|");
			}
			printf("\n");
		}
	}
	
}

//玩家走
void PlayerMove(char board[ROW][COL], int row, int col)
{
	int x = 0, y = 0;
	//只有玩家成功下棋才能跳出循环
	while (1)
	{
		printf("玩家走\n");
		scanf("%d %d", &x, &y);
		//玩家下棋不能越界
		if (x > 0 && x <= ROW && y > 0 && y <= COL)
		{
			//如果玩家选择的位置为空，则可以下棋
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = '*';
				break;
			}
			printf("该位置被占用，请重新输入\n");
		}
		else
			printf("该位置不存在，请重新输入\n");
	}
}


//电脑走（随机）
void ComputerMove(char board[ROW][COL], int row, int col)
{
	printf("电脑走:>\n");
	//电脑下棋成功将空格换为#
	while (1)
	{
		int x = rand() % row;
		int y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
	}
	
}

//判断棋盘是否已满
int IsFull(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			//如果棋盘上还有空格，则表示棋盘未满，返回0
			if (board[i][j] == ' ')
			{
				return 0;
			}	
		}
	}
	//如果棋盘已满返回1
	return 1;
}

//判断游戏是否结束
char GameOver(char board[ROW][COL], int row, int col)
{
	int i = 0, j = 0;
	//在判断有三个相同棋子在一条线上时，应考虑棋盘扩展或者三子棋改为五子棋的情况
	//增强代码的可移植性
	//如果三个相同棋子在一条线上，则返回该棋子的符号
	//玩家赢返回     '*'
	//电脑赢返回     '#'
	//平局返回       'Q'
	//游戏未结束返回 'C'
	//列判断
	for (i = 0; i < row ;i++)
	{
		for (j = 0; j < col - 2; j++)
		{
			if (board[i][j] == board[i][j + 1] && board[i][j + 1] == board[i][j + 2] && board[i][j] != ' ')
			{
				return board[i][j];
			}
		}
	}
	//行判断
	for (i = 0; i < row - 2;i++)
	{
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == board[i + 1][j] && board[i + 1][j] == board[i + 2][j] && board[i][j] != ' ')
			{
				return board[i][j];
			}
		}
	}
	//对角线判断
	for (i = 0; i < row - 2;i++)
	{
		for (j = 0; j < col - 2; j++)
		{
			if (board[i][j] == board[i + 1][j + 1] && board[i + 1][j + 1] == board[i + 2][j + 2] && board[i][j] != ' ')
			{
				return board[i][j];
			}
		}
	}
	for (i = 0; i < row;i++)
	{
		for (j = col - 1; j > 1; j--)
		{
			if (board[i][j] == board[i + 1][j - 1] && board[i + 1][j - 1] == board[i + 2][j - 2] && board[i][j] != ' ')
				return board[i][j];
		}
	}

	//平局
	if (IsFull(board, ROW, COL) == 1)
		return 'Q';
	else
		return 'C';
}