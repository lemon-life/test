#define _CRT_SECURE_NO_WARNINGS 1
#include"LinkStack.h"


void menu()
{
	printf("+----------------------------+\n");
	printf("+           1.入栈           +\n");
	printf("+           2.出栈           +\n");
	printf("+           3.取栈顶元素     +\n");
	printf("+           4.遍历           +\n");
	printf("+           0.退出           +\n");
	printf("+----------------------------+\n");
}
void test()
{
	LiStack S;
	InitLiStack(&S);
	Elem e = 0;
	int input = 0;
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			printf("入栈元素: ");
			scanf("%d", &e);
			Push(&S,e);
			break;
		case 2:
			Pop(&S, &e);
			printf("出栈元素: %d\n", e);
			break;
		case 3:
			GetTop(&S, &e);
			printf("栈顶元素: %d\n", e);
			break;
		case 4:
			printf("遍历元素: ");
			PrintStack(&S);
			break;
		default:
			break;
		}
	} while (input);
}
int main()
{
	test();
	return 0;
}

//int main()
//{
//	int arr[5] = { 0 };
//	int i = 0;
//	while (~scanf("%d", &arr[i]))
//	{
//		if (i >= 5)
//		{
//			break;
//		}
//		i++;
//	}
//	for (i = 0;i < 5;i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//	return 0;
//}