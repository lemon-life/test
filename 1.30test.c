#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

//自除数
int* selfDividingNumbers(int left, int right, int* returnSize)
{
    int i = 0;
    *returnSize = 0;
    int* p = (int*)malloc((right - left + 1) * sizeof(int));
    for (i = left;i <= right;i++)
    {
        int num = i;
        while (num)
        {
            if ((num % 10 == 0) || (i % (num % 10) != 0))
            {
                break;
            }
            else
            {
                num = num / 10;
            }
        }
        if (num == 0)
        {
            p[(*returnSize)++] = i;
        }
    }
    return p;
}
int main()
{
    int left = 0, right = 0;
    while (~scanf("%d %d", &left, &right))
    {
        int arr[100] = { 0 };
        int* p = arr;
        p = selfDividingNumbers(left, right, arr);
        printf("[");
        while (*p > 0 && *p < 10001)
        {
            printf("%d", (*p++));
            if (*p > 0 && *p < 10001)
                printf(",");
        }
        printf("]\n");
    }
    return 0;
}

//多数元素
//int cmp(const void* a, const void* b)
//{
//    return (*(int*)a - *(int*)b);
//}
//int majorityElement(int* nums, int numsSize)
//{
//    qsort(nums, numsSize, sizeof(int), cmp);
//    int mid = numsSize / 2;
//    return nums[mid];
//}
//int main()
//{
//    int nums[1000] = { 0 };
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        for (int i = 0; i < n;i++)
//        {
//            scanf("%d", &nums[i]);
//        }
//        printf("%d", majorityElement(nums, n));
//    }
//	return 0;
//}

//字符个数统计
//int main()
//{
//	char arr[500] = { 0 };
//	while (~scanf("%s", arr))
//	{
//		int flag[128] = { 0 };
//		int sz = strlen(arr);
//		int sum = 0;
//		for (int i = 0;i < sz;i++)
//		{
//			flag[arr[i]] = 1;
//		}
//		for (int j = 0; j < 128;j++)
//		{
//			sum += flag[j];
//		}
//		printf("%d", sum);
//	}
//	return 0;
//}