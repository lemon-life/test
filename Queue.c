#define _CRT_SECURE_NO_WARNINGS 1
#include"Queue.h"

//初始化
void InitQueue(SqQueue* Q)
{
	Q->front = Q->rear = 0;
}
//判断队列是否为空
int Empty(SqQueue* Q)
{
	if (Q->front == Q->rear)
		return TRUE;
	return FALSE;
}
//入队
int EnQueue(SqQueue* Q, Elem x)
{
	if ((Q->rear + 1) % MaxSize == Q->front)
		return FALSE;
	Q->data[Q->rear] = x;
	Q->rear = (Q->rear + 1) % MaxSize;
	return TRUE;
}
//出队
int DeQueue(SqQueue* Q, Elem* x)
{
	if (Empty(Q))
		return FALSE;
	*x = Q->data[Q->front];
	Q->front = (Q->front + 1) % MaxSize;
	return TRUE;
}
//读队头元素
int GetHead(SqQueue* Q, Elem* x)
{
	if (Empty(Q))
		return FALSE;
	*x = Q->data[Q->front];
	return TRUE;
}
//求队列长度
int Length(SqQueue* Q)
{
	return (Q->rear + MaxSize - Q->front) % MaxSize;
}