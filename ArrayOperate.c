#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//初始化数组为全0
void Init(int arr[], int sz)
{
	for (int i = 0; i < sz; i++)
	{
		arr[i] = 0;
	}
}

//打印数组的每个元素
void print(int arr[], int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("arr[%d] = %d ", i, arr[i]);
	}
	printf("\n");
}

//完成数组元素的逆置
void reverse(int arr[], int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		int tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;
		left++;
		right--;
	}
}
int main()
{
	int arr[10];
	int sz = sizeof(arr) / sizeof(arr[0]);
	Init(arr, sz);
	print(arr, sz);
	for(int i = 0; i < sz; i++)
	{
		arr[i] = i + 1;
	}
	print(arr, sz);
	reverse(arr, sz);
	print(arr, sz);
	return 0;
}