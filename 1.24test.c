#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//交换两个变量（不创建临时变量）
//int main()
//{
//	int a = 0, b = 0;
//	while (~scanf("%d %d", &a, &b))
//	{
//		printf("交换前 a = %d, b = %d\n", a, b);
//		a = a ^ b;
//		b = a ^ b;
//		a = a ^ b;
//		printf("交换后 a = %d, b = %d\n", a, b);
//	}
//	return 0;
//}

//二进制中1的个数
//int NumberOf1(int n) {
//    // write code here
//    int count = 0;
//    for (int i = 0; i < 32; i++)
//    {
//        if ((n >> i) & 1)
//            count++;
//    }
//    return count;
//}
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        printf("%d的二进制中1的个数为%d个", n, NumberOf1(n));
//    }
//    return 0;
//}

//打印整数二进制的奇数位和偶数位
//int main()
//{
//	int arr1[16] = { 0 };
//	int arr2[16] = { 0 };
//	int n = 0;
//	while (~scanf("%d", &n))
//	{
//		for (int i = 0;i < 32;i++)
//		{
//			if (i % 2 == 0)
//			{
//				arr1[i / 2] = n >> i & 1;
//			}
//			else
//			{
//				arr2[i / 2] = n >> i & 1;
//			}
//		}
//		int j = 0;
//		printf("奇数位为:>");
//		for (j = 15; j >= 0; j--)
//		{
//			printf("%d ", arr1[j]);
//		}
//		printf("\n偶数位为:>");
//		for (j = 15; j >= 0; j--)
//		{
//			printf("%d ", arr2[j]);
//		}
//		printf("\n");
//	}
//}

//两个整数二进制位不同个数
//int main()
//{
//    int x = 0, y = 0;
//    scanf("%d %d", &x, &y);
//    int count = 0;
//    for (int i = 0; i < 32;i++)
//    {
//        if (((x ^ y) >> i) & 1)
//            count++;
//    }
//    printf("%d", count);
//    return 0;
//}