#define _CRT_SECURE_NO_WARNINGS 1
#include"StringS.h"


//创建串的算法
void StrAssign(SString* str, char cstr[])
{
	int i = 0;
	for (i = 0;cstr[i] != '\0';i++)
	{
		str->ch[i] = cstr[i];
	}
	str->length = i;
}
//复制串的算法
void StrCopy(SString str, SString t)
{
	int i = 0;
	for (i = 0; i < t.length;i++)
	{
		str.ch[i] = t.ch[i];
	}
	str.length = t.length;
}
//判空
int StrEmpty(SString str)
{
	return (str.length == 0);
}
//求串长
int StrLength(SString str)
{
	return str.length;
}
//清空栈操作
void StrClear(SString* str)
{
	str->length = 0;
}
//销毁串
void DestoryStr(SString* str)
{
	free(str);
}
//串连接
SString Concat(SString s, SString t)
{
	SString str;
	int i = 0;
	str.length = s.length + t.length;
	for (i = 0; i < s.length;i++)
	{
		str.ch[i] = s.ch[i];
	}
	for (i = 0; i < t.length;i++)
	{
		str.ch[i + s.length] = t.ch[i];
	}
	return str;
}
//求子串
SString SubStr(SString s, int pos, int len)
{
	SString str;
	int i = 0;
	if (pos < 1 || len < 1 || pos > s.length || pos + len - 1 > s.length)
		return str;
	for (i = 0; i < len; i++)
	{
		str.ch[i] = s.ch[pos + i];
	}
	return str;
}
//比较操作
int StrCompare(SString s, SString t)
{
	int i = 0;
	for (i = 0;i < s.length && i < t.length;i++)
	{
		if (s.ch[i] != t.ch[i])
			return s.ch[i] - t.ch[i];
	}
	return s.length - t.length;
}
//定位操作
int Index(SString s, SString t)
{
	int i = 0;
	SString sub;
	while (i <= s.length - t.length)
	{
		sub = SubStr(s, i, t.length);
		if (StrCompare(sub, t) != 0)
			i++;
		else
			return i;
	}
	return -1;
}
//朴素模式匹配算法(简单模式匹配算法)
int Index2(SString s, SString t)
{
	int i = 0, j = 0, k = 0;
	while (i < s.length && j < t.length)
	{
		if (s.ch[i] == t.ch[j])
		{
			i++;
			j++;
		}
		else
		{
			j = 0;
			k++;
			i = k;
		}
	}
	if (j >= t.length)
		return k;
	return -1;
}
//求模式串T的next数组
void GetNext(SString t, int next[MaxSize])
{
	int i = 1, j = 0;
	next[0] = 0;
	while (i < t.length - 1)
	{
		if (j == 0 || t.ch[i] == t.ch[j])
		{
			i++;
			j++;
			next[i] = j;
		}
		else
		{
			j = next[j];
		}
	}
}
//KMP算法
int IndexKMP(SString s, SString t)
{
	int next[MaxSize];
	int i = 0, j = 0;
	GetNext(t, next);
	while (i < s.length && j < t.length)
	{
		if (j == 0 || s.ch[i] == t.ch[j])
		{
			i++;
			j++;
		}
		else
		{
			j = next[j];
		}
	}
	if (j >= t.length)
		return i;
	return -1;
}
//next数组的优化数组nextval
void GetNextval(SString t, int nextval[MaxSize])
{
	int i = 1, j = 0;
	nextval[0] = 0;
	while (i < t.length)
	{
		if (j == 0 || t.ch[i] == t.ch[j])
		{
			i++;
			j++;
			if (t.ch[i] != t.ch[j])
			{
				nextval[i] = j;
			}
			else
			{
				nextval[i] = nextval[j];
			}
		}
		else
		{
			j = nextval[j];
		}
	}
}
