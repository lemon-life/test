#pragma once
#include<stdio.h>
#include<stdlib.h>
#define SizeInit 10
#define TURE 1
#define FALSE 0

typedef int ElemType;
typedef struct
{
	ElemType* data;
	int length;
	int MaxSize;
}SqList;

//初始化
void InitList(SqList* L);
//动态增加顺序表内存空间
void IncreaseList(SqList* L, int len);
//插入元素
int InsertList(SqList* L, int i, int e);
//删除元素
int ListDelete(SqList* L, int i, int* e);
//顺序表查找第i个元素
ElemType GetElem(SqList L, int i);
//查找顺序表中是否有元素e
int LocateElem(SqList L, int e);

//#define MaxSize 10
//静态顺序表
//typedef int ElemType;//定义数据元素为整型
//typedef struct
//{
//	ElemType data[MaxSize];
//	int length;
//}SqList;
//
//void InitList(SqList* L);


