#define _CRT_SECURE_NO_WARNINGS 1

// 链表的回文结构
class PalindromeList {
public:
    bool chkPalindrome(ListNode* A) {
        // write code here
        if (A == NULL || A->next == NULL)
        {
            return true;
        }
        struct ListNode* slow = A;
        struct ListNode* fast = A;
        struct ListNode* prev = NULL;
        struct ListNode* cur = NULL;
        struct ListNode* curNext = NULL;
        while (fast != NULL && fast->next != NULL)
        {
            slow = slow->next;
            fast = fast->next->next;
        }
        cur = slow;
        while (cur != NULL)
        {
            curNext = cur->next;
            cur->next = prev;
            prev = cur;
            cur = curNext;
        }
        while (prev && A)
        {
            if (prev->val != A->val)
            {
                return false;
            }
            A = A->next;
            prev = prev->next;
        }
        return true;
    }
};