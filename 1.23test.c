#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//空心三角形图案
int main()
{
	int n = 0;
	while (scanf("%d", &n) != EOF)
	{
		for (int i = 0;i<n;i++)
		{
			for (int j = 0;j <= i;j++)
			{
				if (i < 2 || i > n - 2)
				{
					printf("* ");
				}
				else
				{
					if (j == 0 || j == i)
					{
						printf("* ");
					}
					else
						printf("  ");
				}
			}
			printf("\n");
		}
	}
	return 0;
}

//反斜线形图案
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		for (int i = 0;i < n;i++)
//		{
//			for (int j = i;j > 0;j--)
//			{
//				printf(" ");
//			}
//			printf("*\n");
//		}
//	}
//	return 0;
//}

//去重整数并排序
//int main()
//{
//	int num = 0, n = 0;
//	int arr[1001] = { 0 };
//	scanf("%d", &n);
//	while (scanf("%d", &num) != EOF)
//	{
//		arr[num] = num;
//	}
//	for (int i = 0;i < 1001;i++)
//	{
//		if (arr[i] != 0)
//		{
//			printf("%d ", arr[i]);
//		}
//	}
//	return 0;
//}

//翻转金字塔图案
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		for (int i = n;i > 0;i--)
//		{
//			for (int k = n - i;k > 0;k--)
//			{
//				printf(" ");
//			}
//			for (int j = 0; j < i; j++)
//			{
//				printf("* ");
//			}
//			printf("\n");
//		}
//
//	}
//	return 0;
//}