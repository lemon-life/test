#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
void game()
{
	char Mine[ROWS][COLS] = { 0 };
	char Show[ROWS][COLS] = { 0 };
	//先初始化雷区和游戏界面
	InitBoard(Mine, ROWS, COLS, '0');
	InitBoard(Show, ROWS, COLS, '*');
	//布置雷
	SetMine(Mine, ROW, COL);
	//展示隐藏的雷区
	ShowBoard(Show, ROW, COL);
	/*ShowBoard(Mine, ROW, COL);*/
	//游戏操作函数
	FindMine(Mine, Show, ROW, COL);

}
//主菜单
void menu()
{
	printf("+---------------------------+\n");
	printf("|          1. play          |\n");
	printf("|          0. exit          |\n");
	printf("+---------------------------+\n");
}
//测试函数
void test()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择:>\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			system("cls");
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			break;
		}
	} while (input);
}
int main()
{
	test();
	return 0;
}