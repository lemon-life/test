#define _CRT_SECURE_NO_WARNINGS 1
#include"LinkQueue.h"

//初始化(带头节点)
void InitQueue(LinkQueue* Q)
{
	Q->front = Q->rear = (LinkNode*)malloc(sizeof(LinkNode));
	Q->front->next = NULL;
}
//初始化(无头节点)
void InitQueue2(LinkQueue* Q)
{
	Q->front = NULL;
	Q->rear = NULL;
}
//判断是否为空
int Empty(LinkQueue* Q)
{
	if (Q->front->next == NULL)
		return TRUE;
	return FALSE;
}
//入队(有头节点)
void EnQueue(LinkQueue* Q, Elem x)
{
	LinkNode* s = (LinkNode*)malloc(sizeof(LinkNode));
	s->data = x;
	s->next = NULL;
	Q->rear->next = s;
	Q->rear = s;
}
//入队(无头节点)
void EnQueue2(LinkQueue* Q, Elem x)
{
	LinkNode* s = (LinkNode*)malloc(sizeof(LinkNode));
	s->data = x;
	s->next = NULL;
	if (Q->front == NULL)
	{
		Q->front = s;
		Q->rear = s;
	}
	else
	{
		Q->rear->next = s;
		Q->rear = s;
	}
}
//出队(有头节点)
int DeQueue(LinkQueue* Q, Elem* x)
{
	if (Empty(Q))
		return FALSE;
	LinkNode* p = Q->front->next;
	*x = p->data;
	Q->front->next = p->next;
	if (Q->rear == p)
		Q->rear = Q->front;
	free(p);
	return TRUE;
}
//出队(无头节点)
int DeQueue2(LinkQueue* Q, Elem* x)
{
	if (Empty(Q))
		return FALSE;
	LinkNode* p = Q->front;
	*x = p->data;
	Q->front = p->next;
	if (p == Q->rear)
		Q->rear = NULL;
	free(p);
	return TRUE;
}