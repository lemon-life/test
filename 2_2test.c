#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
#include<stdlib.h>

//两数之和
int* twoSum(int* numbers, int numbersLen, int target, int* returnSize) {
    // write code here
    int i = 0, j = 0;
    *returnSize = 2;
    int* p = (int*)malloc(sizeof(int) * 2);
    for (i = 0;i < numbersLen;i++)
    {
        if (numbers[i] > target)
            continue;
        for (j = i + 1;j < numbersLen;j++)
        {
            if (numbers[i] + numbers[j] == target)
            {
                p[0] = i + 1;
                p[1] = j + 1;
                return p;
            }
        }
    }
    return NULL;
}
int main()
{
    int n = 0;
    int num[1000] = { 0 };
    while (~scanf("%d", &n))
    {
        for (int i = 0;i < n;i++)
        {
            scanf("%d", &num[i]);
        }
        int target = 0;
        scanf("%d", &target);
        int* p = (int*)malloc(sizeof(int) * 2);
        p = twoSum(num, n, target, p);
        printf("[%d,%d]\n", p[0], p[1]);
    }
    return 0;
}

//珠玑妙算
//int* masterMind(char* solution, char* guess, int* returnSize)
//{
//    *returnSize = 2;
//    int* p = (int*)malloc(sizeof(int) * 2);
//    int i = 0;
//    int count1 = 0, count2 = 0;
//    int flag[4] = { 0 };
//    for (i = 0;i < 4;i++)
//    {
//        if (solution[i] == guess[i])
//        {
//            count1++;
//            flag[i] = 1;
//        }
//    }
//    p[0] = count1;
//    int flag2[4] = { 0 };
//    for (i = 0;i < 4;i++)
//    {
//        if (flag[i] == 0)
//        {
//            for (int j = 0;j < 4;j++)
//            {
//                if (flag[j] == 1 || flag2[j] == 1)
//                {
//                    continue;
//                }
//                else
//                {
//                    if (solution[i] == guess[j])
//                    {
//                        count2++;
//                        flag2[j] = 1;
//                        break;
//                    }
//                }
//            }
//        }
//    }
//    p[1] = count2;
//    return p;
//}
//int main()
//{
//    char solution[5] = { 0 };
//    char guess[5] = { 0 };
//    while (~scanf("%s %s", solution, guess))
//    {
//        int* p = (int*)malloc(sizeof(int) * 2);
//        p = masterMind(solution, guess, p);
//        printf("[%d,%d]\n", p[0], p[1]);
//    }
//    return 0;
//}

//数列的和
//数列的第一项为n，以后各项为前一项的平方根，求数列的前m项的和。
//double Add(int n, int m)
//{
//	double sum = 0.0;
//	sum = (double)n;
//	double x = sum;
//	while (m > 1)
//	{
//		x = sqrt(x);
//		sum += x;
//		m--;
//	}
//	return sum;
//}
//int main()
//{
//	int n = 0, m = 0;
//	while (~scanf("%d %d", &n, &m))
//	{
//		printf("%.2f\n", Add(n, m));
//	}
//	return 0;
//}

//统计每个月兔子的总数
//有一只兔子，从出生后第3个月起每个月都生一只兔子
//小兔子长到第三个月后每个月又生一只兔子
//假如兔子都不死，问第n个月的兔子总数为多少？
//int RabbitNum(int mon)
//{
//	int a = 1, b = 1, c = 1;
//	while (mon > 2)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		mon--;
//	}
//	return c;
//}
//int main()
//{
//	int month = 0;
//	while (~scanf("%d", &month))
//	{
//		printf("%d\n", RabbitNum(month));
//	}
//	return 0;
//}