#define _CRT_SECURE_NO_WARNINGS 1
#include"LinearList.h"

//顺序表的基本操作

//判断顺序表是否已满
int IsFull(SqList L)
{
	if (L.length == L.MaxSize)
		return TURE;
	else
		return FALSE;
}
//初始化顺序表
void InitList(SqList* L)
{
	L->data = (ElemType*)malloc(sizeof(ElemType) * SizeInit);
	L->length = 0;
	L->MaxSize = SizeInit;
}
//动态增加顺序表内存空间
void IncreaseList(SqList* L, int len)
{
	int* p = L->data;
	L->data = (ElemType*)malloc(sizeof(ElemType) * (L->MaxSize + len));
	L->MaxSize = SizeInit + len;
	for (int i = 0; i < L->length;i++)
	{
		L->data[i] = p[i];
	}
	free(p);
}
//插入元素
int InsertList(SqList* L, int i, int e)
{
	if (IsFull(*L) == 1 || i<1 || i>L->length)
	{
		return FALSE;
	}
	for (int j = L->length;j >= i;j--)
	{
		L->data[j] = L->data[j - 1];
	}
	L->data[i - 1] = e;
	L->length++;
	return TURE;
}
//删除元素
int ListDelete(SqList* L, int i, int* e)
{
	if (i < 1 || i > L->length)
		return FALSE;
	*e = L->data[i - 1];
	for (int j = i - 1;j < L->length;j++)
	{
		L->data[j] = L->data[j + 1];
	}
	L->length--;
	return TURE;
}
//顺序表查找第i个元素
ElemType GetElem(SqList L, int i)
{
	return L.data[i - 1];
}
//查找顺序表中是否有元素e
int LocateElem(SqList L, int e)
{
	int i = 0;
	for (i = 0;i < L.length;i++)
	{
		if (L.data[i] == e)
		{
			return i + 1;
		}
	}
	return 0;
}

//void InitList(SqList* L)
//{
//	L->length = 0;
//	for (int i = 0;i < MaxSize;i++)
//	{
//		L->data[i] = 0;
//	}
//}
