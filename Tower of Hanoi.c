#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//汉诺塔实现
void Hanoi(int n, char A, char B, char C)
{
	if (n == 1)
	{
		printf("%c->%c ", A, C);
	}
	else
	{
		Hanoi(n - 1, A, C, B);//将B视为C，C视为B
		printf("%c->%c ", A, C);
		Hanoi(n - 1, B, A, C);//将B视为A，A视为B
	}
}
int main()
{
	int n = 0;
	printf("请输入汉诺塔的层数:>\n");
	scanf("%d", &n);
	Hanoi(n, 'A', 'B', 'C');
	return 0;
}