#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#define ROW 9
#define COL 9
#define ROWS ROW + 2
#define COLS COL + 2
//雷数需小于等于ROW*COL
#define MINES 10

//1.标记
//2.展开一片

//初始化棋盘
void InitBoard(char arr[ROWS][COLS], int row, int col, char set);

//打印棋盘
void ShowBoard(char show[ROWS][COLS], int row, int col);

//布置雷
void SetMine(char show[ROWS][COLS], int row, int col);

//排查雷
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col);

//标记雷
void SignMine(char show[ROWS][COLS], int row, int col);
