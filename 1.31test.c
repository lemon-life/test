#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

//找到所有数组中消失的数字
int* findDisappearedNumbers(int* nums, int numsSize, int* returnSize)
{
    *returnSize = 0;
    int i = 0;
    int* p = (int*)malloc(sizeof(int) * numsSize);
    int* t = (int*)malloc(sizeof(int) * numsSize);
    for (i = 0;i < numsSize;i++)
    {
        p[i] = 0;
    }
    for (i = 0;i < numsSize;i++)
    {
        p[nums[i] - 1] = 1;
    }
    for (i = 0;i < numsSize;i++)
    {
        if (p[i] == 0)
        {
            t[(*returnSize)++] = i + 1;
        }
    }
    return t;
}
int main()
{
    int nums[1000] = { 0 };
    int n = 0;
    while (~scanf("%d", &n))
    {
        int i = 0;
        int* p = (int*)malloc(sizeof(int) * n);
        for (i = 0; i < n;i++)
        {
            scanf("%d", &nums[i]);
        }
        p = findDisappearedNumbers(nums, n, p);
        i = 0;
        while (p[i] > 0 && p[i] < 100001)
        {
            printf("%d ", p[i]);
            i++;
        }
        printf("\n");
    }
    return 0;
}

//
//int Add(int num1, int num2) {
//    // write code here
//    int m = 0, n = 0;
//    m = (num1 & num2) << 1;
//    n = (num1 ^ num2);
//    while (m & n)
//    {
//        num1 = m;
//        num2 = n;
//        m = (num1 & num2) << 1;
//        n = num1 ^ num2;
//    }
//    return m | n;
//}
//int main()
//{
//    int num1 = 0, num2 = 0;
//    while (~scanf("%d %d", &num1, &num2))
//    {
//        printf("%d+%d=%d\n", num1, num2, Add(num1, num2));
//    }
//	return 0;
//}

//除自身以外数组的乘积
//int* productExceptSelf(int* nums, int numsSize, int* returnSize)
//{
//    *returnSize = numsSize;
//    int* p = (int*)malloc((numsSize) * sizeof(int));
//    int i = 0;
//    int left = 1, right = 1;
//    for (i = 0; i < numsSize;i++)
//    {
//        p[i] = left;
//        left *= nums[i];
//    }
//    for (i = numsSize - 1;i >= 0;i--)
//    {
//        p[i] *= right;
//        right *= nums[i];
//    }
//    return p;
//}
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        int i = 0;
//        int* p = (int*)malloc(n * sizeof(int));
//        int nums[1000] = { 0 };
//        for (i = 0;i < n;i++)
//        {
//            scanf("%d", &nums[i]);
//        }
//        p = productExceptSelf(nums, n, p);
//        for (i = 0;i < n;i++)
//        {
//            printf("%d ", p[i]);
//        }
//        printf("\n");
//        free(p);
//    }
//    return 0;
//}