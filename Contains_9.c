#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//包含数字9的数,表示1~2019中共有多少个数包含数字9。
int main()
{
	int i = 0;
	int count = 0;
	for (i = 1;i <= 2019;i++)
	{
		if (i % 10 == 9 || i / 1000 == 9 || (i/10) % 10 == 9 || (i / 100) % 10 == 9)
		{
			count++;
		}
	}
	printf("%d\n", count);
	return 0;
}