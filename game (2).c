#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

//初始化棋盘
//多一个参数set便于布置雷和打印棋盘
void InitBoard(char arr[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0, j = 0;
	for (i = 0; i < rows;i++)
	{
		for (j = 0;j < cols;j++)
		{
			arr[i][j] = set;
		}
	}
}

//打印棋盘
void ShowBoard(char show[ROWS][COLS], int row, int col)
{
	printf("------------------扫雷------------------\n");
	int i = 0, j = 0;
	printf("   |");
	for (i = 1;i <= row;i++)
	{
		printf(" %d ", i);
		if (i<row)
		{
			printf("|");
		}
	}
	printf("\n");
	for (i = 1;i <= row;i++)
	{
		for (j = 0;j <= col;j++)
		{
			printf("---");
			if (j < col)
			{
				printf("|");
			}
		}
		printf("\n %d |", i);
		for (j = 1;j <= col;j++)
		{
			printf(" %c ", show[i][j]);
			if (j < col)
			{
				printf("|");
			}
		}
		printf("\n");
	}
	printf("------------------扫雷------------------\n");
}

//布置雷
void SetMine(char mine[ROWS][COLS], int row, int col)
{
	int count = MINES;
	int x = 0, y = 0;
	//每布置一个雷count自减1，直到雷布置完
	while (count)
	{
		x = rand() % row + 1;
		y = rand() % row + 1;
		if (mine[x][y] == '0')
		{
			mine[x][y] = '1';
			count--;
		}
	}
}

//判断游戏是否结束
int IsWin(char show[ROWS][COLS], int row, int col)
{
	int i = 0, j = 0;
	int count = 0;
	for (i = 1;i <= row;i++)
	{
		for (j = 1;j <= col;j++)
		{
			//只有该位置为*或者#，才记为剩余的可操作区域，
			//当剩余的可操作区域等于雷数时则游戏获胜
			if (show[i][j] == '*' || show[i][j] == '#')
			{
				count++;
			}
		}
	}
	//若未打开区域和标记区域等于雷数则返回0，否则返回1
	if (count == MINES)
		return 0;
	else
		return 1;
}


//计算周围雷的个数
int GetMines(char mine[ROWS][COLS], int x, int y)
{
	return mine[x-1][y-1] +
		mine[x-1][y] +
		mine[x-1][y+1] +
		mine[x][y-1] +
		mine[x][y+1] +
		mine[x+1][y-1] +
		mine[x+1][y] +
		mine[x+1][y+1] - 8 * '0';

}

//展开周围都没有雷的雷盘（扩展式排雷）
void ExpandMine(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y)
{
	//如果该位置周围8快没雷，则对周围8块检索,展开
	if (GetMines(mine, x, y) == 0)
	{
		//展开的位置都置为0
		show[x][y] = '0';
		int i = 0;
		int j = 0;
		for (i = x - 1; i <= x + 1; i++)
		{
			for (j = y - 1; j <= y + 1; j++)
			{
				//只有该位置未被打开才能检索，并且检索位置应在棋盘之内
				if (show[i][j] == '*' && i > 0 && i <= ROW && j > 0 && j <= COL)
				{
					ExpandMine(mine, show, i, j);
				}
			}
		}
	}
	else
	{
		//不需要展开则显示附近雷的个数
		show[x][y] = GetMines(mine, x, y) + '0';
	}
}

//排查雷
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0, y = 0;
	while (IsWin(show, row, col))
	{
		int input = 0;
		//1.排查雷
		//2.标记雷
		//每次操作可以选择是排查雷还是标记雷
		do
		{
			printf("请选择操作1.排查雷 2.标记雷:>");
			scanf("%d", &input);
			switch (input)
			{
				//排查雷
			case 1:
				printf("请输入坐标;>");
				scanf("%d %d", &x, &y);
				if (x > 0 && x <= row && y > 0 && y <= col && show[x][y] != '#')
				{
					if (mine[x][y] == '1')
					{
						printf("很遗憾你被炸死了\n");
						ShowBoard(mine, ROW, COL);
						//被炸死直接退出
						return ;
					}
					else
					{
						ExpandMine(mine, show, x, y);
						system("cls");
						ShowBoard(show, ROW, COL);
					}
				}
				else
				{
					//如果该位置被标记则无法排查
					if (show[x][y] == '#')
					{
						printf("该位置被标记为雷，请重新输入\n");
					}
					//如果排查位置越界则报错
					else
						printf("坐标非法，请重新输入\n");
				}
				break;
				//标记雷
			case 2:
				SignMine(show, row, col);
				system("cls");
				ShowBoard(show, ROW, COL);
				break;
			default:
				break;
			}
		} while (IsWin(show, row, col));//当玩家获胜或是踩雷时循环结束
	}
	//如果玩家扫雷成功，则显示布雷图
	if (!IsWin(show, row, col))
	{
		system("cls");
		printf("恭喜您扫雷成功!:>\n");
		ShowBoard(mine, ROW, COL);
	}
}

//标记雷
void SignMine(char show[ROWS][COLS], int row, int col)
{
	int x = 0, y = 0;
	printf("请输入坐标;>");
	scanf("%d %d", &x, &y);
	//标记雷
	if (show[x][y] == '*')
	{
		show[x][y] = '#';
	}
	//取消标记
	else if (show[x][y] == '#')
	{
		show[x][y] = '*';
	}
	//若该位置被打开则无法标记
	else
		printf("标记失败");
}