#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
int* printNumbers(int n, int* returnSize) {
    *returnSize = pow(10, n) - 1;
    int* a = (int*)malloc((*returnSize) * sizeof(int));
    for (int i = 0; i < *returnSize; i++)
    {
        *(a + i) = i + 1;
    }
    return a;
}
int main()
{
    int arr[] = { 0 };
    int n = 0;
    int* a;
    scanf("%d", &n);
    a = printNumbers(n, arr);
    printf("[");
    for (int i = 0;i<pow(10,n) - 1;i++)
    {
        printf("%d", *(a+i));
        if (i != pow(10, n) - 2)
            printf(",");
    }
    printf("]");
    return 0;
}