#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
#include<stdlib.h>

//错误的集合
//集合 s 包含从 1 到?n?的整数。
//不幸的是，因为数据错误，导致集合里面某一个数字复制了成了集合里面的另外一个数字的值
//导致集合丢失了一个数字并且有一个数字重复 。
//给定一个数组 nums 代表了集合 S 发生错误后的结果。
//请你找出重复出现的整数，再找到丢失的整数，将它们以数组的形式返回。
int* findErrorNums(int* nums, int numsSize, int* returnSize) {
    int i = 0;
    if (*nums == 2)
    {
        *returnSize = 2;
        *(returnSize + 1) = 1;
        return returnSize;
    }
    else
    {
        while (*(nums + i) == (i + 1))
        {
            i++;
        }
        *returnSize = *(nums + i);
        *(returnSize + 1) = (i + 1);
        return returnSize;
    }
}
//int* findErrorNums(int* nums, int numsSize, int* returnSize) {
//    int i = 0;
//    *returnSize = 2;
//    int* p = (int *)malloc((*returnSize) * sizeof(int));
//    if (*nums == 2)
//    {
//        *p = 2;
//        *(p + 1) = 1;
//        return p;
//    }
//    else
//    {
//        while (*(nums + i) == (i + 1))
//        {
//            i++;
//        }
//        *p = *(nums + i);
//        *(p + 1) = (i + 1);
//        return p;
//    }
//}
int main()
{
    int str[2] = { 0 };
    int arr[4] = { 1,2,2,4 };
    int* p = findErrorNums(arr, 4, str);
    printf("[%d,%d]\n", str[0], str[1]);
	return 0;
}

//旋转数组的最小数字
//有一个长度为 n 的非降序数组，比如[1,2,3,4,5]，将它进行旋转
//即把一个数组最开始的若干个元素搬到数组的末尾，变成一个旋转数组
//比如变成了[3,4,5,1,2]，或者[4,5,1,2,3]这样的。
//请问，给定这样一个旋转数组，求数组中的最小值。
//int minNumberInRotateArray(int* rotateArray, int rotateArrayLen) {
//    // write code here
//    int p = *rotateArray;
//    int i = 0;
//    while ((p >= *(rotateArray + rotateArrayLen - 1 - i)) && (rotateArrayLen - 1 - i) > 0)
//    {
//        i++;
//        if (*(rotateArray + rotateArrayLen - 1 - i) < *(rotateArray + rotateArrayLen - 2 - i))
//        {
//            break;
//        }
//    }
//    if ((p <= *(rotateArray + rotateArrayLen - 1 - i)) && i > 0)
//        i--;
//    if ((p < *(rotateArray + rotateArrayLen - 1 - i)) && i == 0)
//        return p;
//    else
//        return *(rotateArray + rotateArrayLen - 1 - i);
//}
//int main()
//{
//    int arr[] = { 2,2,2,1,2 };
//    printf("%d",minNumberInRotateArray(arr, 5));
//	return 0;
//}

//打印菱形
//int main()
//{
//	//选择菱形的层数
//	int n = 0;
//	while (~scanf("%d", &n))
//	{
//		int i = 0, j = 0, k = 0;
//		for (i = 0;i < n;i++)
//		{
//			for (j = n - i - 1;j > 0;j--)
//			{
//				printf(" ");
//			}
//			for (k = 0;k < 2*i+1;k++)
//			{
//				printf("*");
//			}
//			printf("\n");
//		}
//		for (i = n;i > 1;i--)
//		{
//			for (j = 1;j <= n - i + 1;j++)
//			{
//				printf(" ");
//			}
//			for (k = 0;k < 2 * i - 3;k++)
//			{
//				printf("*");
//			}
//			printf("\n");
//		}
//	}
//	return 0;
//}

//打印水仙花数
//int NarcissisticNumber(int n)
//{
//	int sum = 0;
//	while (n)
//	{
//		sum += pow(n % 10,3);
//		n = n / 10;
//	}
//	return sum;
//}
//int main()
//{
//	int i = 0;
//	for (i = 0;i <= 100000;i++)
//	{
//		if (i == NarcissisticNumber(i))
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//计算求和
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int sum = 0, i = 0;
//	int n = 0;
//	for (i = 0; i < 5; i++)
//	{
//		n = n * 10 + a;
//		sum += n;
//	}
//	printf("%d", sum);
//	return 0;
//}

//字符逆序
//void Inverse(char* str, int n)
//{
//	int i = 0;
//	while((str + i)< (str + n - i - 1))
//	{
//		char tmp = *(str + i);
//		*(str + i) = *(str + n - i - 1);
//		*(str + n - i - 1) = tmp;
//		i++;
//	}
//}
//int main()
//{
//	char str[10000] = { 0 };
//	gets(str);
//	int sz = strlen(str);
//	Inverse(str, sz);
//	puts(str);
//	return 0;
//}

//使用指针打印数组内容
//void print(int* str,int n)
//{
//	for(int i = 0;i<n;i++)
//	{
//		printf("%d ", *(str+i));
//	}
//}
//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr,sz);
//	return 0;
//}