#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"

//检查扩容
void checkCapacity(SeqList* ps)
{
	assert(ps);
	if (ps->size == ps->capacity)
	{
		SLDateType* tmp = (SLDateType*)realloc(ps->a, sizeof(SLDateType) * (ps->capacity + IncrSize));
		if (tmp == NULL)
		{
			printf("顺序表已满，增容失败\n");
			return;
		}
		ps->a = tmp;
	}
}

//顺序表初始化
void SeqListInit(SeqList* ps)
{
	assert(ps);
	ps->size = 0;
	ps->capacity = InitSize;
	ps->a = (SLDateType*)malloc(sizeof(SLDateType) * ps->capacity);
}

//销毁顺序表
void SeqListDestory(SeqList* ps)
{
	assert(ps);
	ps->size = 0;
	ps->capacity = 0;
	free(ps->a);
}

//打印顺序表
void SeqListPrint(SeqList* ps)
{
	assert(ps);
	int i = 0;
	for (i = 0;i < ps->size;i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

//尾插
void SeqListPushBack(SeqList* ps, SLDateType x)
{
	assert(ps);
	checkCapacity(ps);
	ps->a[ps->size] = x;
	ps->size++;
}

//头插
void SeqListPushFront(SeqList* ps, SLDateType x)
{
	assert(ps);
	checkCapacity(ps);
	for (int i = ps->size - 1;i >= 0;i--)
	{
		ps->a[i + 1] = ps->a[i];
	}
	ps->a[0] = x;
	ps->size++;
}

//头删
void SeqListPopFront(SeqList* ps)
{
	assert(ps);
	assert(ps->size);
	for (int i = 1; i < ps->size;i++)
	{
		ps->a[i - 1] = ps->a[i];
	}
	ps->size--;
}

//尾删
void SeqListPopBack(SeqList* ps)
{
	assert(ps);
	assert(ps->size);
	ps->size--;
}

// 顺序表查找
int SeqListFind(SeqList* ps, SLDateType x)
{
	assert(ps);
	int pos = 0;
	for (pos = 0; pos < ps->size;pos++)
	{
		if (ps->a[pos] == x)
		{
			return pos;
		}
	}
	return -1;
}

// 顺序表在pos位置插入x
void SeqListInsert(SeqList* ps, size_t pos, SLDateType x)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	checkCapacity(ps);
	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->a[end + 1] = ps->a[end];
		--end;
		if (end == pos)//pos类型是无符号整型，故pos为0时end--会陷入死循环
			break;
	}
	ps->a[pos] = x;
	ps->size++;
}

// 顺序表删除pos位置的值
void SeqListErase(SeqList* ps, size_t pos)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);
	int i = pos;
	while (i < ps->size - 1)
	{
		ps->a[i] = ps->a[i + 1];
		i++;
	}
	ps->size--;
}

void test1()
{
	/*SeqList s1;
	SeqListInit(&s1);
	SeqListPushBack(&s1, 8);
	SeqListPushBack(&s1, 7);
	SeqListPushBack(&s1, 6);
	SeqListPushBack(&s1, 5);
	SeqListPushBack(&s1, 4);
	SeqListPushBack(&s1, 3);
	SeqListPushBack(&s1, 2);
	SeqListPushBack(&s1, 1);
	SeqListPrint(&s1);
	SeqListDestory(&s1);*/
}

void test2()
{
	/*SeqList s2;
	SeqListInit(&s2);
	SeqListPushFront(&s2, 8);
	SeqListPushFront(&s2, 7);
	SeqListPushFront(&s2, 6);
	SeqListPushFront(&s2, 5);
	SeqListPushFront(&s2, 4);
	SeqListPushFront(&s2, 3);
	SeqListPushFront(&s2, 2);
	SeqListPushFront(&s2, 1);
	SeqListPrint(&s2);
	SeqListDestory(&s2);*/
}

void test3()
{
	/*SeqList s3;
	SeqListInit(&s3);
	SeqListPushFront(&s3, 8);
	SeqListPushFront(&s3, 7);
	SeqListPushFront(&s3, 6);
	SeqListPushFront(&s3, 5);
	SeqListPushFront(&s3, 4);
	SeqListPushFront(&s3, 3);
	SeqListPushFront(&s3, 2);
	SeqListPushFront(&s3, 1);
	SeqListPrint(&s3);

	SeqListPopFront(&s3);
	SeqListPrint(&s3);
	SeqListPopFront(&s3);
	SeqListPrint(&s3);
	SeqListPopFront(&s3);
	SeqListPrint(&s3);
	SeqListPopFront(&s3);
	SeqListPrint(&s3);
	SeqListPopFront(&s3);
	SeqListPrint(&s3);
	SeqListPopFront(&s3);
	SeqListPrint(&s3);
	SeqListPopFront(&s3);
	SeqListPrint(&s3);
	SeqListPopFront(&s3);
	SeqListPrint(&s3);
	SeqListDestory(&s3);*/
}

void test4()
{
	/*SeqList s3;
	SeqListInit(&s3);
	SeqListPushFront(&s3, 8);
	SeqListPushFront(&s3, 7);
	SeqListPushFront(&s3, 6);
	SeqListPushFront(&s3, 5);
	SeqListPushFront(&s3, 4);
	SeqListPushFront(&s3, 3);
	SeqListPushFront(&s3, 2);
	SeqListPushFront(&s3, 1);
	SeqListPrint(&s3);

	SeqListPopBack(&s3);
	SeqListPrint(&s3);
	SeqListPopBack(&s3);
	SeqListPrint(&s3);
	SeqListPopBack(&s3);
	SeqListPrint(&s3);
	SeqListPopBack(&s3);
	SeqListPrint(&s3);
	SeqListPopBack(&s3);
	SeqListPrint(&s3);
	SeqListPopBack(&s3);
	SeqListPrint(&s3);
	SeqListPopBack(&s3);
	SeqListPrint(&s3);
	SeqListPopBack(&s3);
	SeqListPrint(&s3);
	SeqListDestory(&s3);*/
}

void test5()
{
	/*SeqList s3;
	SeqListInit(&s3);
	SeqListPushFront(&s3, 8);
	SeqListPushFront(&s3, 7);
	SeqListPushFront(&s3, 6);
	SeqListPushFront(&s3, 5);
	SeqListPushFront(&s3, 4);
	SeqListPushFront(&s3, 3);
	SeqListPushFront(&s3, 2);
	SeqListPushFront(&s3, 1);
	SeqListPrint(&s3);
	printf("9在顺序表中的位置是%d\n", SeqListFind(&s3, 9));
	SeqListDestory(&s3);*/
}

void test6()
{
	/*SeqList s;
	SeqListInit(&s);
	SeqListPushFront(&s, 8);
	SeqListPushFront(&s, 7);
	SeqListPushFront(&s, 6);
	SeqListPushFront(&s, 5);
	SeqListPushFront(&s, 4);
	SeqListPrint(&s);
	SeqListInsert(&s, 0, 40);
	SeqListPrint(&s);*/
}
void test7()
{
	SeqList s;
	SeqListInit(&s);
	SeqListPushFront(&s, 8);
	SeqListPushFront(&s, 7);
	SeqListPushFront(&s, 6);
	SeqListPushFront(&s, 5);
	SeqListPushFront(&s, 4);
	SeqListPrint(&s);
	SeqListErase(&s, 2);
	SeqListPrint(&s);
}