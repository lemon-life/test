#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

class Person
{
private:
	char Name[8];
	char Sex;//性别：1为男，2为女
	char Birth[10];
public:
	Person()
	{
		cout << "调用Person构造函数" << endl;
	}
	Person(char name[], char sex, char birth[])
	{
		strcpy(Name, name);
		Sex = sex;
		strcpy(Birth, birth);
	}
	void Show()
	{
		cout <<"姓名:"<< Name << " 生日:" << Birth << endl;
	}
};

class Student :public Person
{
private:
	char Sclass[10]; //班级
	int No; //学号
	char Major[10]; //专业
	float Eng, Math; //英语，数学成绩
public:
	Student()
	{
		cout << "调用Student构造函数" << endl;
	}
	Student(char name[], char sex, char birth[], char sclass[], int no, char major[], float eng, float math):Person(name,sex,birth)
	{
		strcpy(Sclass, sclass);
		No = no;
		strcpy(Major, major);
		Eng = eng;
		Math = math;
	}
	void Print()
	{
		Show();
		cout << "班级:" << Sclass << " 专业:" << Major << endl;
	}
};
class Employee :public Person
{
private:
	char Department[10]; //部门
	char Title[10]; //头衔
	float Salary; //薪水
public:
	Employee()
	{
		cout << "调用Employee构造函数" << endl;
	}
	Employee(char name[], char sex, char birth[], char department[10], char title[10], float salary) :Person(name, sex, birth)
	{
		strcpy(Department, department);
		strcpy(Title, title);
		Salary = salary;
	}
	void Print()
	{
		Show();
		cout << "部门:" << Department << " 头衔:" << Title << endl;
	}
};
int main(void)
{
	int no;
	char name[8], sex, birth[10], major[10], class1[10], depa[10], title[10];
	float eng, math, salary;
	Student* s;
	Employee* e;
	cout << "Input a student data" << '\n';
	cin >> name >> sex >> birth >> class1 >> no >> major >> eng >> math;
	s = new Student(name, sex, birth, class1, no, major, eng, math);
	cout << "The student information:\n";
	s->Print();
	cout << "Input an employee data" << '\n';
	cin >> name >> sex >> birth >> depa >> title >> salary;
	e = new Employee(name, sex, birth, depa, title, salary);
	cout << "The employee information:\n";
	e->Print();
	return 0;
}