#pragma once
#include<stdio.h>
#include<stdlib.h>

//静态
#define MaxSize 255
typedef struct
{
	char ch[MaxSize];
	int length;
}SString;

//动态
typedef struct
{
	char* ch;
	int length;
}HString;

//链式存储
typedef struct StringNode
{
	char ch;//每个节点存一个字符
	struct StringNode* next;
}SNode, StringNode;

//高密度的链式存储
typedef struct HStringNode
{
	char ch[4];
	struct HStringNode* next;
}HSNode, HStringNode;