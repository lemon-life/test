#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//乘法口诀表
void MultiplicationTable(int n)
{
	for (int i = 1;i <= n;i++)
	{
		for (int j = 1;j <= i;j++)
		{
			printf("%-2d*%-2d=%-3d ", i, j, i * j);
		}
		printf("\n");
	}
}
int main()
{
	int n = 0;
	printf("请输入您想打印的乘法口诀表层数:>\n");
	scanf("%d", &n);
	MultiplicationTable(n);
	return 0;
}
//交换两个整数
//void swap(int* x, int* y)
//{
//	int tmp = *x;
//	*x = *y;
//	*y = tmp;
//}
//int main()
//{
//	int a = 0, b = 0;
//	printf("请输入两个整数:>\n");
//	scanf("%d %d", &a, &b);
//	printf("交换前:a = %d, b = %d\n", a, b);
//	swap(&a, &b);
//	printf("交换后:a = %d, b = %d\n", a, b);
//	return 0;
//}

//函数判断闰年
//int IsLeapYear(int x)
//{
//	if (((x % 4 == 0) && (x % 100 != 0)) || (x % 400 == 0))
//	{
//		return 1;
//	}
//	return 0;
//}
//int main()
//{
//	int year = 0;
//	printf("请输入年份:>\n");
//	scanf("%d", &year);
//	if (IsLeapYear(year))
//	{
//		printf("%d年是闰年\n", year);
//	}
//	else
//		printf("%d年不是闰年\n", year);
//	return 0;
//}

//#include<math.h>
////实现一个函数，判断一个数是不是素数。
//int IsPrime(int x)
//{
//	for (int i = 2; i <= sqrt(x);i++)
//	{
//		if (x % i == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//}
//int main()
//{
//	for (int i = 100; i <= 200; i++)
//	{
//		if (IsPrime(i))
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}


//二分查找
//int main() 
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz - 1;
//	printf("请输入要找的数:>\n");
//	scanf("%d", &k);
//	while (left <= right)
//	{
//		int mid = left + (right - left) / 2;
//		if (k > arr[mid])
//		{
//			left = mid + 1;
//		}
//		else if (k < arr[mid])
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			printf("找到了,下标为:%d\n", mid);
//			break;
//		}
//	}
//	if (left > right)
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}

//#include<time.h>
////猜数字游戏
//void game()
//{
//	int num = rand() % 100 + 1;
//	int a = 0;
//	while (1)
//	{
//		printf("猜数字:\n");
//		scanf("%d", &a);
//		if (a > num)
//		{
//			printf("猜大了\n");
//		}
//		else if (a < num)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("猜对了\n");
//			break;
//		}
//	}
//	
//}
//void menu()
//{
//	printf("*****************\n");
//	printf("**** 1.play  ****\n");
//	printf("**** 0.exit  ****\n");
//	printf("*****************\n");
//}
//int main()
//{
//	srand((unsigned int)time(NULL));
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:>\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			break;
//		default:
//			break;
//		}
//	} while (input);
//	return 0;
//}


//9*9乘法口诀表
//int main()
//{
//	for (int i = 1;i <= 9;i++)
//	{
//		for (int j = 1;j <= i;j++)
//		{
//			printf("%d*%d = %-2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//求10 个整数中最大值
//int main()
//{
//	int arr[10] = { 0 };
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int max = arr[0];
//	for (int j = 0; j < 10; j++)
//	{
//		if (arr[j] > max)
//		{
//			max = arr[j];
//		}
//	}
//	printf("最大值是:%d\n", max);
//	return 0;
//}

//计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值，打印出结果
//int main()
//{
//	float sum = 0.0;
//	int flag = 1;
//	for (int i = 1; i <= 100; i++)
//	{
//		sum += flag * (1.0 / i);
//		flag = -flag;
//	}
//	printf("%f\n", sum);
//	return 0;
//}

//1到 100 的所有整数中出现多少个数字9
//int main()
//{
//	int count = 0;
//	for (int i = 1; i < 100;i++)
//	{
//		
//		if (i / 10 == 9)
//		{
//			count++;
//		}
//		if (i % 10 == 9)
//		{
//			count++;
//		}
//	}
//	printf("count = %d\n", count);
//	return 0;
//}

//打印100~200之间的素数
//int main()
//{
//	for (int i = 100; i <= 200;i++)
//	{
//		int j = 2;
//		for (j; j <= sqrt(i);j++)
//		{
//			if (i % j == 0)
//			{
//				break;
//			}
//
//		}
//		if (j > sqrt(i))
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//打印1000年到2000年之间的闰年
//int main()
//{
//	for (int i = 1000;i<=2000;i+=4)
//	{
//		if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//最小公倍数

//给定两个数，求这两个数的最大公约数
//int main()
//{
//	int a = 0, b = 0;
//	scanf("%d %d", &a, &b);
//	while (a % b)
//	{
//		int x = a % b;
//		a = b;
//		b = x;
//	}
//	printf("最大公约数为:%d\n", b);
//	return 0;
//}

//打印1-100之间所有3的倍数的数字
//int main()
//{
//	for (int i = 3; i < 100; i += 3)
//	{
//		printf("%d ", i);
//	}
//	return 0;
//}

//将三个整数数按从大到小输出
//int main()
//{
//	int x = 0, y = 0, z = 0;
//	scanf("%d %d %d", &x, &y, &z);
//	if (y > x)
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	}
//	if (z > x)
//	{
//		int tmp = x;
//		x = z;
//		z = tmp;
//	}
//	if (z > y)
//	{
//		int tmp = y;
//		y = z;
//		z = tmp;
//	}
//	printf("%d %d %d\n", x, y, z);
//	return 0;
//}