#pragma once
#include<stdio.h>
#include<stdlib.h>
#define FALSE 0
#define TRUE 1


typedef int ElemType;
//双链表结构体
typedef struct DNode
{
	ElemType data;
	struct DNode* next, * prior;
}DNode, *DLinkList;

//初始化
int InitLink(DLinkList* L);
//判断是否为空
int Empty(DLinkList L);
//按位查找
DNode* GetElem(DLinkList L, int i);
//按值查找
DNode* LocateElem(DLinkList L, ElemType e);
//前插法
int InsertPriorList(DNode* p, DNode* s);
//后插法
int InsertNextList(DNode* p, DNode* s);
//删除第i个节点
int Delete(DLinkList* L, int i, ElemType* e);