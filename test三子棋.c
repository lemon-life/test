#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
//游戏函数
void game()
{
	char board[ROW][COL] = { 0 };
	InitBoard(board, ROW, COL);
	DisplayBoard(board, ROW, COL);
	char ret = 0;
	//游戏操作,当双方未到达获胜条件或是棋盘未满时游戏继续
	while (1)
	{
		PlayerMove(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		ret = GameOver(board, ROW, COL);
		if (ret != 'C')
			break;
		ComputerMove(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		ret = GameOver(board, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
		printf("恭喜您赢了！\n");
	else if (ret == '#')
		printf("很遗憾，您输了\n");
	else
		printf("平局\n");
	DisplayBoard(board, ROW, COL);
		
}
//游戏菜单
void menu()
{
	printf("+-------------------------+\n");
	printf("|         1. play         |\n");
	printf("|         0. exit         |\n");
	printf("+-------------------------+\n");
}
//测试函数
void test()
{
	int input = 0;
	//调用时间戳
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择\n");
		scanf("%d", &input);
		//1.游戏  0.退出游戏
		switch (input)
		{
		case 1:
			printf("三子棋\n");
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			break;
		}
	} while (input);
}
int main()
{
	
	test();
	return 0;
}