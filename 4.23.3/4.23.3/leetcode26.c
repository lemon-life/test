#define _CRT_SECURE_NO_WARNINGS 1
//删除排序数组中的重复项
int removeDuplicates(int* nums, int numsSize) {
    assert(nums);
    int dest = 0;
    int src = 1;
    while (src < numsSize)
    {
        if (nums[dest] == nums[src]) {
            src++;
        }
        else {
            nums[++dest] = nums[src++];
        }
    }
    return dest + 1;
}