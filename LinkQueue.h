#pragma once
#include<stdio.h>
#include<stdlib.h>

#define TRUE 1
#define FALSE 0
typedef int Elem;
typedef struct LinkNode
{
	Elem data;
	struct LinkNode *next;
}LinkNode;
typedef struct
{
	LinkNode* front, * rear;
}LinkQueue;

//初始化(带头节点)
void InitQueue(LinkQueue* Q);
//初始化(无头节点)
void InitQueue2(LinkQueue* Q);
//判断是否为空
int Empty(LinkQueue* Q);
//入队(有头节点)
void EnQueue(LinkQueue* Q, Elem x);
//入队(无头节点)
void EnQueue2(LinkQueue* Q, Elem x);
//出队(有头节点)
int DeQueue(LinkQueue* Q, Elem* x);
//出队(无头节点)
int DeQueue2(LinkQueue* Q, Elem* x);