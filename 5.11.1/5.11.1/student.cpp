#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;
using std::string;


class student 
{
public:
	student():id(nullptr)
	{
		age = 0;
		name = nullptr;
		sex = nullptr;
		cout << "调用无参构造函数" << endl;
	}
	student(const student& stu):id(stu.id)
	{
		age = stu.age;
		name = stu.name;
		sex = stu.sex;
		cout << "调用拷贝构造函数" << endl;
	}
	student(string name, string Id, string sex, int age = 18):id(Id)
	{
		this->age = age;
		this->name = name;
		this->sex = sex;
		cout << "调用默认参数构造函数" << endl;
	}
	student(string Name, string Id, string Sex, int Age) :id(Id)
	{
		age = Age;
		name = Name;
		sex = Sex;
		cout << "调用构造函数" << endl;
	}
	~student()
	{
		
		cout << "对象被释放" << endl;
	}
private:
	int age;
	string name;
	const string id;
	string sex;
	static string school;
};

string student::school = "云南大学";