#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>

//求三位数中质数的个数
//int main()
//{
//    int count = 0;
//    for (int i = 100;i <= 999;i++)
//    {
//        int j = 0;
//        //一个数的两个因子一定有一个比他的平方根小，故只循环到sqrt(i)
//        for (j = 2; j <= sqrt(i);j++)
//        {
//            //不是素数则跳出循环
//            if (i % j == 0)
//            {
//                break;
//            }
//        }
//        //如果j > sqrt(i)则说明该 i 为素数，计数count自加1
//        if (j > sqrt(i))
//        {
//            count++;
//        }
//    }
//    printf("%d\n", count);
//    return 0;
//}

//递归和非递归分别实现求第n个斐波那契数
//递归
//int Fibonacci1(int n)
//{
//	if (n > 2)
//		return (Fibonacci1(n - 1) + Fibonacci1(n - 2));
//	else
//		return 1;
//}
////非递归
//int Fibonacci2(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while(n > 2)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//int main()
//{
//	int n = 0;
//	printf("请输入要求第几个斐波那契数:>\n");
//	scanf("%d", &n);
//	printf("第%d个斐波那契数 = %d\n", n, Fibonacci1(n));
//	printf("第%d个斐波那契数 = %d\n", n, Fibonacci2(n));
//	return 0;
//}

//递归实现n的k次方
//int MyPow(int n, int k)
//{
//	int mul = 1;
//	if (k > 0)
//	{
//		mul = n * MyPow(n, k - 1);
//	}
//	return mul;
//}
//int main()
//{
//	int num = 0;
//	int k = 0;
//	printf("请输入一个数和要求的次方:>\n");
//	scanf("%d %d", &num, &k);
//	printf("%d的%d次方 = %d\n", num, k, MyPow(num, k));
//	return 0;
//}

//计算一个数的每位之和（递归实现）
//int DigitSum(int n)
//{
//	int sum = n % 10;
//	if (n > 9)
//	{
//		sum = sum + DigitSum(n / 10);
//	}
//	return sum;
//}
//int main()
//{
//	int num = 0;
//	printf("请输入一个整数:>\n");
//	scanf("%d", &num);
//	printf("%d的每位之和 = %d", num, DigitSum(num));
//	return 0;
//}

//字符串逆序（递归实现）
//void reverse_string(char* string)
//{
//	char tmp = *string;//保存字符串的首字符
//	//将字符串的尾字符赋给首字符
//	char* p = string;
//	while (*p != '\0')
//	{
//		p = p + 1;
//
//	}
//	p = p - 1;
//	*string = *p;
//	//将字符串尾置为\0
//	*(p) = '\0';
//	//字符串长度大于一就递归
//	if (*(string+1) != '\0')
//	{
//		reverse_string(string + 1);
//	}
//	//将存于tmp中的首字符置于尾字符
//	*p = tmp;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	reverse_string(arr);
//	printf("%s\n", arr);
//	return 0;
//}