#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//�������� II
struct ListNode* detectCycle(struct ListNode* head) {
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    while (fast != NULL)
    {
        slow = slow->next;
        if (fast->next == NULL)
        {
            return NULL;
        }
        fast = fast->next->next;
        if (fast == slow)
        {
            struct ListNode* cur = head;
            while (cur != slow)
            {
                cur = cur->next;
                slow = slow->next;
            }
            return cur;
        }
    }
    return NULL;
}