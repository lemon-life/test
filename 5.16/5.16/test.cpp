#define _CRT_SECURE_NO_WARNINGS 1
//�����ָ�
class Partition {
public:
    ListNode* partition(ListNode* pHead, int x) {
        // write code here
        if (pHead == NULL)
        {
            return NULL;
        }
        struct ListNode* headA = (struct ListNode*)malloc(sizeof(struct ListNode));
        struct ListNode* headB = (struct ListNode*)malloc(sizeof(struct ListNode));
        struct ListNode* lastA = headA;
        struct ListNode* lastB = headB;
        struct ListNode* cur = pHead;
        while (cur != NULL)
        {
            if (cur->val < x)
            {
                lastA->next = cur;
                lastA = cur;
            }
            else
            {
                lastB->next = cur;
                lastB = cur;
            }
            cur = cur->next;
        }
        lastA->next = headB->next;
        lastB->next = NULL;
        pHead = headA->next;
        return pHead;
    }
};