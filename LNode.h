#pragma once
#include<stdio.h>
#include<stdlib.h>
#define FALSE 0
#define TRUE 1;

typedef int ElemType;
typedef struct LNode
{
	ElemType data;
	struct Node *next;
}LNode, *LinkList;
//初始化单链表
int InitList(LinkList L);
//判断单链表是否为空
int Empty(LinkList L);
//按位查找
LNode* GetElem(LinkList L, int i);
//按值查找
LNode* LocateElem(LinkList L, ElemType e);
//插入元素
int ListInsert(LinkList L, int i, ElemType e);
//后插操作
int InsertNextNode(LNode* p, ElemType e);
//前插操作
int InsertPriorNode(LNode* p, ElemType e);
//按位删除元素
int ListDelete(LinkList L, int i, ElemType* e);
//删除指定节点
int DeleteNode(LNode* p);
//求表长
int Length(LinkList L);