#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//�ཻ����
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
    int lenA = 0;
    int lenB = 0;
    int len = 0;
    struct ListNode* pl = headA;
    struct ListNode* ps = headB;
    while (pl != NULL)
    {
        lenA++;
        pl = pl->next;
    }
    pl = headA;
    while (ps != NULL)
    {
        lenB++;
        ps = ps->next;
    }
    ps = headB;
    len = lenA - lenB;
    if (len < 0)
    {
        len = lenB - lenA;
        pl = headB;
        ps = headA;
    }
    while (len > 0)
    {
        pl = pl->next;
        len--;
    }
    while (pl != NULL && ps != NULL)
    {
        if (pl == ps)
        {
            return pl;
        }
        pl = pl->next;
        ps = ps->next;
    }
    return NULL;
}