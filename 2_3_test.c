#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>

//截取字符串
int main()
{
    char arr[100] = { 0 };
    while (~scanf("%s", arr))
    {
        int n = 0;
        scanf("%d", &n);
        /*for (int i = 0;i < n;i++)
        {
            printf("%c", arr[i]);
        }
        printf("\n");*/
        arr[n] = '\0';
        printf("%s", arr);
    }
    return 0;
}

//数对
//x和y均不大于n, 并且x除以y的余数大于等于k。
//计算一共有多少个可能的数对。
//long Logarithm(int n, int k)
//{
//    int i = 0, j = 0;
//    long count = 0;
//    if (k == 0 || k == n - 1)
//        return pow(n - k, 2);
//    for (j = k + 1; j <= n; j++)
//    {
//        count += ((n / j) * (j - k)) + ((n % j < k) ? 0 : (n % j - k + 1));
//    }
//    return count;
//}
//int main()
//{
//    int n = 0, k = 0;
//    while (~scanf("%d %d", &n, &k))
//    {
//        printf("%ld\n", Logarithm(n, k));
//    }
//    return 0;
//}

//寻找峰值
//int findPeakElement(int* nums, int numsLen)
//{
//    // write code here
//    int i = 0;
//    for (i = 0;i < numsLen; i++)
//    {
//        if (nums[i] > nums[i - 1] && nums[i] > nums[i + 1])
//        {
//            return i;
//        }
//    }
//    return 0;
//}
////二分思想
//int findPeakElement2(int* nums, int numsLen)
//{
//    int left = 0;
//    int right = numsLen - 1;
//    if (numsLen == 1 || nums[0] > nums[1])
//        return 0;
//    if (nums[right - 1] < nums[right])
//        return right;
//    while (left < right)
//    {
//        int mid = left + (right - left) / 2;
//        if (nums[mid] > nums[mid + 1])
//        {
//            right = mid;
//        }
//        else
//            left = mid + 1;
//    }
//    return left;
//}
//int main()
//{
//    int n = 0;
//    int num[200000] = { 0 };
//    while (~scanf("%d", &n))
//    {
//        for (int i = 0;i < n;i++)
//        {
//            scanf("%d", &num[i]);
//        }
//        printf("%d\n", findPeakElement2(num, n));
//    }
//	return 0;
//}

//寻找奇数
//int main()
//{
//	int n = 0;
//	while (~scanf("%d", &n))
//	{
//		int i = 0, ret = 0, num = 0;
//		for (i = 0;i < n;i++)
//		{
//			scanf("%d", &ret);
//			num ^= ret;
//		}
//		printf("%d\n", num);
//	}
//	return 0;
//}
