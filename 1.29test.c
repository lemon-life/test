#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//寻找数组的中心下标
int pivotIndex(int* nums, int numsSize) 
{
    int total = 0;
    int i = 0;
    for (i = 0;i < numsSize;i++)
    {
        total += nums[i];
    }
    int sum = 0;
    for (i = 0;i < numsSize;i++)
    {
        if ((2 * sum + nums[i]) == total)
            return i;
        sum += nums[i];
    }
    return -1;
}
int main()
{
    int n = 0;
    int nums[1000] = { 0 };
    while (~scanf("%d", &n))
    {
        for (int i = 0;i < n;i++)
        {
            scanf("%d", &nums[i]);
        }
        printf("%d\n", pivotIndex(nums, n));
    }
    return 0;
}

//图片整理
//int cmp(const void* a, const void* b)
//{
//    return (* (char*)a - *(char*)b);
//}
//
//int main()
//{
//    char arr[1024] = { 0 };
//    while (~scanf("%s", arr))
//    {
//        qsort(arr,strlen(arr),sizeof(char),cmp);
//        printf("%s\n", arr);
//    }
//    return 0;
//}