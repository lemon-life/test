#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
typedef struct Stu
{
	int Num;
	char name[20];
	int Math;
	int English;
	int Chinese;
}Stu;

int main()
{
	Stu a[3];
	int i = 0;
	float b[3] = { 0.0 };
	while (i<3)
	{
		scanf("%d %s %d %d %d", &a[i].Num, a[i].name, &a[i].Math, &a[i].English, &a[i].Chinese);
		b[i] = (a[i].Math + a[i].English + a[i].Chinese) / 3.0;
		i++;
	}
	FILE* fp;
	fp = fopen("stud.txt", "a+");
	if (fp == NULL)
	{
		printf("打开文件失败\n");
		exit(1);
	}
	for (i = 0; i < 3;i++)
	{
		fprintf(fp, "%d\t%s\t%d\t%d\t%d\t%f\n", a[i].Num, a[i].name, a[i].Math, a[i].English, a[i].Chinese, b[i]);
	}
	fclose(fp);
	float c[3] = { 0.0 };
	FILE* tp;
	tp = fopen("stud.txt", "r");
	if(tp == NULL)
	{
		printf("打开文件失败\n");
		exit(1);
	}
	int count = 0;
	for (i = 0; i < 3; i++)
	{
		fscanf(tp, "%d\t%s\t%d\t%d\t%d\t%f", &a[i].Num, a[i].name, &a[i].Math, &a[i].English, &a[i].Chinese, &c[i]);
		if (c[i] > 80.0)
			count++;
	}
	printf("%d\n", count);
	fclose(fp);
	return 0;
}